# Quick setup

#### 1. Include library

``` groovy
repositories {
  ...
  maven { url  "https://gitlab.com/iria_somobu/m2/-/raw/master/" }
}

dependencies {
  ...
  implementation 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
}
```


#### 2. Android Manifest
``` xml
<manifest>
	<!-- Include following permission if you load images from Internet -->
	<uses-permission android:name="android.permission.INTERNET" />
	<!-- Include following permission if you want to cache images on SD card -->
	<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
	...
</manifest>
```

#### 3. Application or Activity class (before the first usage of ImageLoader)
``` java
public class MyActivity extends Activity {
	@Override
	public void onCreate() {
		super.onCreate();

		// Create global configuration and initialize ImageLoader with this config
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
			...
			.build();
		ImageLoader.getInstance().init(config);
		...
	}
}
```
