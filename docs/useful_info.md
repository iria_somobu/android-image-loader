# Useful Info

## Caching is NOT enabled by default

If you want loaded images to be cached in memory and/or on disk then you should enable caching in DisplayImageOptions this way:
```java
// Create default options which will be used for every 
//  displayImage(...) call if no options will be passed to this method
DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
	...
    .cacheInMemory(true)
    .cacheOnDisk(true)
    ...
    .build();
ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
    ...
    .defaultDisplayImageOptions(defaultOptions)
    ...
    .build();
ImageLoader.getInstance().init(config); // Do it on Application start
```

```java
// Then later, when you want to display image
ImageLoader.getInstance().displayImage(imageUrl, imageView); // Default options will be used
```

or this way:
```java
DisplayImageOptions options = new DisplayImageOptions.Builder()
	...
    .cacheInMemory(true)
    .cacheOnDisk(true)
    ...
    .build();
ImageLoader.getInstance().displayImage(imageUrl, imageView, options); // Incoming options will be used
```


## External storage caching

If you enabled disk caching then UIL try to cache images on external storage (/sdcard/Android/data/[package_name]/cache). If external storage is not available then images are cached on device's filesystem.

To provide caching on external storage (SD card) add following permission to AndroidManifest.xml:
``` java
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" android:maxSdkVersion="18" />
```


## Bitmap size calculation

How UIL define Bitmap size needed for exact ImageView?

It searches defined parameters:
 * Get actual measured width and height of ImageView
 * Get `android:layout_width` and `android:layout_height` parameters
 * Get `android:maxWidth` and/or `android:maxHeight` parameters
 * Get maximum width and/or height parameters from configuration (`memoryCacheExtraOptions(int, int)` option)
 * Get width and/or height of device screen

So **try to set** `android:layout_width`|`android:layout_height` or `android:maxWidth`|`android:maxHeight` parameters for ImageView if you know approximate maximum size of it. It will help correctly compute Bitmap size needed for this view and **save memory**.


## On OutOfMemoryErrors

If you often got **OutOfMemoryError** in your app using Universal Image Loader then disable caching in memory. If OOM is still occurs then it seems your app has a memory leak. Use MemoryAnalyzer to detect it.

Otherwise try the following steps (all of them or several):
 - Reduce thread pool size in configuration (`.threadPoolSize(...)`). 1 - 5 is recommended.
 - Use `.bitmapConfig(Bitmap.Config.RGB_565)` in display options. Bitmaps in RGB_565 consume 2 times less memory than in ARGB_8888.
 - Use `.imageScaleType(ImageScaleType.EXACTLY)`
 - Use `.diskCacheExtraOptions(480, 320, null)` in configuration

## On memory cache

For memory cache configuration (`ImageLoaderConfiguration.memoryCache(...)`) you can use already prepared implementations.
 * Cache using **only strong** references:
     * `LruMemoryCache` (Least recently used bitmap is deleted when cache size limit is exceeded) - **Used by default**
 * Caches using **weak and strong** references:
     * `UsingFreqLimitedMemoryCache` (Least frequently used bitmap is deleted when cache size limit is exceeded)
     * `LRULimitedMemoryCache` (Least recently used bitmap is deleted when cache size limit is exceeded)
     * `FIFOLimitedMemoryCache` (FIFO rule is used for deletion when cache size limit is exceeded)
     * `LargestLimitedMemoryCache` (The largest bitmap is deleted when cache size limit is exceeded)
     * `LimitedAgeMemoryCache` (Decorator. Cached object is deleted when its age exceeds defined value)
 * Cache using **only weak** references:
     * `WeakMemoryCache` (Unlimited cache)


## On disk cache
For disk cache configuration (`ImageLoaderConfiguration.diskCache(...)`) you can use already prepared implementations:
 * `UnlimitedDiscCache` (The fastest cache, doesn't limit cache size) - **Used by default**
 * `LruDiskCache` (Cache limited by total cache size and/or by file count. If cache size exceeds specified limit then least-recently used file will be deleted)
 * `LimitedAgeDiscCache` (Size-unlimited cache with limited files' lifetime. If age of cached file exceeds defined limit then it will be deleted from cache.)
 
**NOTE:** UnlimitedDiscCache is pretty faster than other limited disk cache implementations.


## Other
To display bitmap (`DisplayImageOptions.displayer(...)`) you can use already prepared implementations: 
 * `RoundedBitmapDisplayer` (Displays bitmap with rounded corners)
 * `FadeInBitmapDisplayer` (Displays image with "fade in" animation)

To avoid list (grid, ...) scrolling lags you can use `PauseOnScrollListener`:
```java
boolean pauseOnScroll = false; // or true
boolean pauseOnFling = true; // or false
PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader, pauseOnScroll, pauseOnFling);
listView.setOnScrollListener(listener);
```

If you see in logs some strange supplement at the end of image URL (e.g. `http://anysite.com/images/image.png_230x460`) then it doesn't mean this URL is used in requests. This is just "URL + target size", also this is key for Bitmap in memory cache. This postfix (`_230x460`) is **NOT used in requests**.

ImageLoader always keeps aspect ratio of images.

