# How to build library

1. Install gradle ([there is APT repo][gradle_apt]);
2. Build library with `gradle jarRelease` from `library` folder;
3. Grab library from `library/build/libs/library.jar`.


# How to build sample

TBD


[gradle_apt]: https://launchpad.net/~cwchien/+archive/ubuntu/gradle
