# Universal Image Loader

The great ancestor of modern image-loading libraries :)
UIL aims to provide a powerful, flexible and highly customizable instrument for image loading, caching and displaying. It provides a lot of configuration options and good control over the image loading and caching process.

![Screenshot](https://gitlab.com/iria_somobu/android-image-loader/-/raw/master/docs/res/UniversalImageLoader.png)

## Features
 * Lighweight
 * Android 4.1+ support
 * Multi-thread image loading (async or sync)
 * Wide customization of ImageLoader's configuration (thread executors, downloader, decoder, memory and disk cache, display image options, etc.)
 * Many customization options for every display image call (stub images, caching switch, decoding options, Bitmap processing and displaying, etc.)
 * Image caching in memory and/or on disk (device's file system or SD card)
 * Listening loading process (including downloading progress)

## Documentation
 * **[Quick Setup][doc_setup]**
 * **[Configuration][doc_config]**
 * **[Display Options][doc_disopt]**
 * [Useful Info][doc_useinfo] - Read it before asking a question
 * [Sample project][dir_sample] - Learn it to understand the right way of library usage
 * [Changelog][doc_chlog] - Info about API changes is here

[doc_chlog]: https://gitlab.com/iria_somobu/android-image-loader/-/blob/master/CHANGELOG.md
[doc_setup]: https://gitlab.com/iria_somobu/android-image-loader/-/blob/master/docs/quick_setup.md
[doc_config]: https://gitlab.com/iria_somobu/android-image-loader/-/blob/master/docs/configuration.md
[doc_disopt]: https://gitlab.com/iria_somobu/android-image-loader/-/blob/master/docs/display_options.md
[doc_useinfo]: https://gitlab.com/iria_somobu/android-image-loader/-/blob/master/docs/useful_info.md
[dir_sample]: https://gitlab.com/iria_somobu/android-image-loader/-/tree/master/sample


## Usage

### Dependency

Repo:
```
maven { url  "https://gitlab.com/iria_somobu/m2/-/raw/master/" }
```

Dependency:
```
implementation 'somobu:image-loader:1.9.5'
```

### Acceptable URIs examples
``` java
"http://site.com/image.png" // from Web
"file:///mnt/sdcard/image.png" // from SD card
"file:///mnt/sdcard/video.mp4" // from SD card (video thumbnail)
"content://media/external/images/media/13" // from content provider
"content://media/external/video/media/13" // from content provider (video thumbnail)
"assets://image.png" // from assets
"drawable://" + R.drawable.img // from drawables (non-9patch images)
```
**NOTE:** Use `drawable://` only if you really need it! Always **consider the native way** to load drawables - `ImageView.setImageResource(...)` instead of using of `ImageLoader`.

### Simple
``` java
ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
```
``` java
// Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view 
//	which implements ImageAware interface)
imageLoader.displayImage(imageUri, imageView);
```
``` java
// Load image, decode it to Bitmap and return Bitmap to callback
imageLoader.loadImage(imageUri, new SimpleImageLoadingListener() {
	@Override
	public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
		// Do whatever you want with Bitmap
	}
});
```
``` java
// Load image, decode it to Bitmap and return Bitmap synchronously
Bitmap bmp = imageLoader.loadImageSync(imageUri);
```

### Complete
``` java
// Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view 
//	which implements ImageAware interface)
imageLoader.displayImage(imageUri, imageView, options, new ImageLoadingListener() {
	@Override
	public void onLoadingStarted(String imageUri, View view) {
		...
	}
	@Override
	public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
		...
	}
	@Override
	public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
		...
	}
	@Override
	public void onLoadingCancelled(String imageUri, View view) {
		...
	}
}, new ImageLoadingProgressListener() {
	@Override
	public void onProgressUpdate(String imageUri, View view, int current, int total) {
		...
	}
});
```
``` java
// Load image, decode it to Bitmap and return Bitmap to callback
ImageSize targetSize = new ImageSize(80, 50); // result Bitmap will be fit to this size
imageLoader.loadImage(imageUri, targetSize, options, new SimpleImageLoadingListener() {
	@Override
	public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
		// Do whatever you want with Bitmap
	}
});
```
``` java
// Load image, decode it to Bitmap and return Bitmap synchronously
ImageSize targetSize = new ImageSize(80, 50); // result Bitmap will be fit to this size
Bitmap bmp = imageLoader.loadImageSync(imageUri, targetSize, options);
```



## License

    Copyright 2011-2015 Sergey Tarasevich

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

